Name:           cairo
Version:        1.18.4
Release:        1
Summary:        A 2D graphics library
License:        LGPL-2.1-only OR MPL-1.1
URL:            https://cairographics.org
Source0:        https://cairographics.org/releases/%{name}-%{version}.tar.xz

BuildRequires:  gcc
BuildRequires:  meson >= 1.3.0
BuildRequires:  pkgconfig(fontconfig) >= 2.13.0
BuildRequires:  pkgconfig(freetype2) >= 23.0.17
BuildRequires:  pkgconfig(glib-2.0) >= 2.14
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(libpng) >= 1.4.0
BuildRequires:  pkgconfig(lzo2)
BuildRequires:  pkgconfig(pixman-1) >= 0.40.0
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(x11-xcb)
BuildRequires:  pkgconfig(xcb) >= 1.6
BuildRequires:  pkgconfig(xcb-render) >= 1.6
BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(xrender)
BuildRequires:  pkgconfig(zlib)
BuildRequires:  gtk-doc

%description
Cairo is a 2D graphics libarary with support for multiple output devices.
It provides high-quality display and print output and this package also
contains functionality to make cairo graphics library integrate well with
GObject used by GNOME.

%package        devel
Summary: Development files for cairo
Requires: %{name}%{?_isa} = %{version}-%{release}

%description    devel
This package contains libraries, header files and developer documentation
needed for developing software which uses the cairo graphics library and
cairo GObject library and contains tools for working with the cairo graphics
library as well.

%package gobject
Summary: GObject bindings for cairo
Requires: %{name}%{?_isa} = %{version}-%{release}
 
%description gobject
Cairo is a 2D graphics library designed to provide high-quality display
and print output.
 
This package contains functionality to make cairo graphics library
integrate well with the GObject object system used by GNOME.
 
%package gobject-devel
Summary: Development files for cairo-gobject
Requires: %{name}-devel%{?_isa} = %{version}-%{release}
Requires: %{name}-gobject%{?_isa} = %{version}-%{release}
 
%description gobject-devel
Cairo is a 2D graphics library designed to provide high-quality display
and print output.
 
This package contains libraries, header files and developer documentation
needed for developing software which uses the cairo Gobject library.
 
%package tools
Summary: Development tools for cairo
Requires: %{name}%{?_isa} = %{version}-%{release}

%description tools
Cairo is a 2D graphics library designed to provide high-quality display
and print output.
 
This package contains tools for working with the cairo graphics library.
 * cairo-trace: Record cairo library calls for later playback

%prep
%autosetup -p1

%build
%meson \
  -Dfreetype=enabled \
  -Dfontconfig=enabled \
  -Dglib=enabled \
  -Dgtk_doc=true \
  -Dspectre=disabled \
  -Dsymbol-lookup=disabled \
  -Dtee=enabled \
  -Dtests=disabled \
  -Dxcb=enabled \
  -Dxlib=enabled
%meson_build

%install
%meson_install

%files
%license COPYING COPYING-LGPL-2.1 COPYING-MPL-1.1
%doc AUTHORS BUGS NEWS README.md
%{_libdir}/libcairo.so.2*
%{_libdir}/libcairo-script-interpreter.so.2*
 
%files devel
%dir %{_includedir}/cairo/
%{_includedir}/cairo/cairo-deprecated.h
%{_includedir}/cairo/cairo-features.h
%{_includedir}/cairo/cairo-ft.h
%{_includedir}/cairo/cairo.h
%{_includedir}/cairo/cairo-pdf.h
%{_includedir}/cairo/cairo-ps.h
%{_includedir}/cairo/cairo-script-interpreter.h
%{_includedir}/cairo/cairo-svg.h
%{_includedir}/cairo/cairo-tee.h
%{_includedir}/cairo/cairo-version.h
%{_includedir}/cairo/cairo-xlib-xrender.h
%{_includedir}/cairo/cairo-xlib.h
%{_includedir}/cairo/cairo-script.h
%{_includedir}/cairo/cairo-xcb.h
%{_libdir}/libcairo.so
%{_libdir}/libcairo-script-interpreter.so
%{_libdir}/pkgconfig/cairo-fc.pc
%{_libdir}/pkgconfig/cairo-ft.pc
%{_libdir}/pkgconfig/cairo.pc
%{_libdir}/pkgconfig/cairo-pdf.pc
%{_libdir}/pkgconfig/cairo-png.pc
%{_libdir}/pkgconfig/cairo-ps.pc
%{_libdir}/pkgconfig/cairo-script-interpreter.pc
%{_libdir}/pkgconfig/cairo-svg.pc
%{_libdir}/pkgconfig/cairo-tee.pc
%{_libdir}/pkgconfig/cairo-xlib.pc
%{_libdir}/pkgconfig/cairo-xlib-xrender.pc
%{_libdir}/pkgconfig/cairo-script.pc
%{_libdir}/pkgconfig/cairo-xcb-shm.pc
%{_libdir}/pkgconfig/cairo-xcb.pc
%{_datadir}/gtk-doc/html/cairo
 
%files gobject
%{_libdir}/libcairo-gobject.so.2*
 
%files gobject-devel
%{_includedir}/cairo/cairo-gobject.h
%{_libdir}/libcairo-gobject.so
%{_libdir}/pkgconfig/cairo-gobject.pc
 
%files tools
%{_bindir}/cairo-trace
%{_libdir}/cairo/

%changelog
* Fri Mar 14 2025 Funda Wang <fundawang@yeah.net> - 1.18.4-1
- update to 1.18.4

* Tue Sep 03 2024 Funda Wang <fundawang@yeah.net> - 1.18.2-1
- update to 1.18.2

* Wed Dec 27 2023 Paul Thomas <paulthomas100199@gmail.com> - 1.18.0-1
- Type: requirement
- ID: NA
- SUG: NA
- DESC: update to 1.18.0

* Wed Jul 26 2023 zhangqiumiao<zhangqiumiao1@huawei.com> - 1.17.8-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update to 1.17.8

* Wed Jun 14 2023 sunhai<sunhai10@huawei.com> - 1.17.4-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:cairo_truetype_reverse_cmap detected memory leaks
       fix read memory access
       fix call get_unaligned_be32 heap buffer overflow
       fix heap buffer overflow in cairo_cff_parse_charstring

* Thu Dec  1 2022 pengyi<pengyi37@huawei.com> - 1.17.4-3
- DESC:correct source URL

* Mon May 09 2022 wangkerong<wangkerong@h-partners.com> - 1.17.4-2
- DESC:disable symbol-lookup,fix build failure after install binutils-devel

* Mon Nov 22 2021 hanhui<hanhui15@huawei.com> - 1.17.4-1
- DESC:update to 1.17.4

* Wed May 26 2021 liuyumeng <liuyumeng5@huawei.com> - 1.16.0-5
- Add a BuildRequires for gcc

* Thu Apr 1 2021 wangkerong <wangkerong@huawei.com> - 1.16.0-4
- Type:cve
- ID:CVE-2020-35492 
- SUG:NA
- DESC:fix CVE-2020-35492

* Sun Sep 27 2020 wangye <wangye70@huawei.com> - 1.16.0-3
- fix source URL

* Fri Sep 18 2020 yanglu <yanglu60@huawei.com> - 1.16.0-2
- Type:cves
- ID:CVE-2019-6461 CVE-2019-6462
- SUG:NA
- DESC:fix CVE-2019-6461 CVE-2019-6462

* Mon Jul 13 2020 jinzhimin <jinzhimin2@huawei.com> - 1.16.0-1
- Version upgrade

* Fri Jan 10 2020 zhangrui <zhangrui182@huawei.com> - 1.15.14-3
- Remove unnecessary patch

* Thu Sep 19 2019 Alex Chao <zhaolei746@huawei.com> - 1.15.14-2
- Package init
